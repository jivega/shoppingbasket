package com.jivega.shopping.shoppingbasket

import com.jivega.shopping.shoppingbasket.Shopping.{Basket, Good}
import org.scalatest.FlatSpec
import com.jivega.shopping.shoppingbasket.Shopping.{apple, bread, milk, soup, listDiscount}

class BasketTest extends FlatSpec {


  val shopping1: Basket = Map(apple -> 4, bread -> 1, milk -> 1)
  val shopping2: Basket = Map(apple -> 1, milk -> 1, bread -> 1)
  val shopping3: Basket = Map(soup -> 2, milk -> 1, bread -> 1, apple -> 1)
  val shopping4: Basket = Map(soup -> 2, milk -> 1, bread -> 1)

  val listDiscount1: Array[Discount] = Array(new PercentageItem(apple, 10.0))

  val output1 = Shopping.makeTotal(shopping1, listDiscount1)
  println("out1: " + output1)

  "The string Discount in shopping1 " should "have the same value" in {
    val testoutput1 = "Subtotal: £ 6.10\nApples 10.0 off: 0.4\nTotal price: 5.70"
    assert(output1 === testoutput1)
  }
  val output2 = Shopping.makeTotal(shopping2, listDiscount1)
  println("out2: " + output2)
  "The string Discount in shopping2 " should "have the same value" in {
    val testoutput2 = "Subtotal: £ 3.10\nApples 10.0 off: 0.1\nTotal price: 3.00"
    assert(output2 === testoutput2)
  }
  val output3 = Shopping.makeTotal(shopping3, listDiscount)
  "The string Discount in shopping3 " should "have the same value" in {
    val testoutput3 = "Subtotal: £ 4.40\nApples 10.0 off: 0.1\nBread 50.0 off: 0.4\nTotal price: 3.90"
    assert(output3 === testoutput3)
  }
  val output4 = Shopping.makeTotal(shopping4, listDiscount)
  "The string Discount in shopping4 " should "have the same value" in {
    val testoutput4 = "Subtotal: £ 3.40\nBread 50.0 off: 0.4\nTotal price: 3.00"
    assert(output4 === testoutput4)
  }
}
