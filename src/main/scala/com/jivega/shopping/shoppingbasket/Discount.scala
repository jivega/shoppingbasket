package com.jivega.shopping.shoppingbasket

import com.jivega.shopping.shoppingbasket.Shopping.{Basket, Good}

trait Discount {
  def makeDiscount(basket: Basket): Option[MessageDiscount]
}

case class MessageDiscount(message: String, discount: Double)

case class PercentageItem(good: Good, discount: Double) extends Discount {
  override def makeDiscount(basket: Basket): Option[MessageDiscount] = {
    basket.get(good) match {
      case Some(i) => Some(new MessageDiscount(good.name + " " + discount.toString + " off: " + i * discount / 100, i * discount / 100))
      case None => None
    }
  }
}

case class PercentageTwoItems(ammountCond: Integer, goodCond: Good, ammountDisc: Integer, goodDisc: Good, discount: Double) extends Discount {
  override def makeDiscount(basket: Basket): Option[MessageDiscount] = {
    if ((basket(goodCond) >= ammountCond) && (basket(goodDisc) >= ammountDisc)) {
      val valueDiscount: Double = (basket(goodCond) / ammountCond) * basket(goodDisc) * goodDisc.price * discount / 100
      Some(new MessageDiscount(goodDisc.name + " " + discount + " off: " + valueDiscount, valueDiscount))
    }
    else None
  }
}
