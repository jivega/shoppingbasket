package com.jivega.shopping.shoppingbasket

object Shopping {
  type Basket = Map[Good, Int]

  case class Good(id: String, name: String, price: Double)

  val apple: Good = new Good("APL", "Apples", 1.00)
  val soup: Good = new Good("SOUP", "Soup", 0.65)
  val milk: Good = new Good("MILK", "Milk", 1.30)
  val bread: Good = new Good("BREAD", "Bread", 0.80)

  val catalog: Map[String, Good] = Map("Apple" -> apple, "Soup" -> soup, "Milk" -> milk, "Bread" -> bread)

  val listDiscount: Array[Discount] = Array(new PercentageItem(apple, 10.0),
    new PercentageTwoItems(2, soup, 1, bread, 50))

  def main(args: Array[String]): Unit = {
    val basketShop = for (arg <- args) yield catalog(arg)
    val basket = basketShop.groupBy(i => i).map(i => (i._1, i._2.length))
    val output: String = makeTotal(basket, listDiscount)
    println(output)
  }

  def makeTotal(basket: Basket, listDiscount: Array[Discount]): String = {
    val sb = StringBuilder.newBuilder
    val subtotal: Double = basket.map(x => x._1.price * x._2).reduce(_ + _)
    sb.append("Subtotal: £ " + "%.2f".format(subtotal).toString)
    sb.append("\n")
    val listMessage = listDiscount.flatMap(x => x.makeDiscount(basket))
    if (listMessage.length == 0) {
      sb.append("(No Offers Available)\n")
    } else {
      for (message <- listMessage) {
        sb.append(message.message + "\n")
      }
    }
    val totalDisc: Double = if (listMessage.length == 0) (0.0) else listMessage.map(x => x.discount).reduce(_ + _)
    val totalPrice: Double = (subtotal - totalDisc)
    sb.append("Total price: " + "%.2f".format(totalPrice))
    sb.toString()
  }

}
